# Reproduction of Android issue 230595

This is a minimal project which shows the bug described in [Issue 230595: BottomNavigationView: TTS engine not reading out title of menu entires.](https://code.google.com/p/android/issues/detail?id=230595).

Once built and installed, enable the "TalkBack" accessibility service in your Android device. This is done by navigating to the System settings -> Accessibility -> TalkBack.
[The play store](https://play.google.com/store/apps/details?id=com.google.android.marvin.talkback) includes some screenshots of how to enable it when viewing com.google.android.marvin.talkback.

For this particular issue, the developer was using the version of TalkBack [provided by F-Droid](https://f-droid.org/repository/browse/?fdid=com.google.android.marvin.talkback).

With TalkBack enabled, open this proof of concpet app.

 * You should see five items in the `BottomNavigationView`, the first of which has a label below it and the other four will just have icons.
 * Long press on the first item. It should (correctly) read out the text below the icon.
 * Now long press on any other item. It will fail to read out anything, making it difficult to navigate for visually impaired users.
